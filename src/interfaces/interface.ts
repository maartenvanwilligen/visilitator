export interface Project {
  projectId : string;
  title: string;
  now: string;
  desirable: string;
  proposal: string;
  step: string;
  version: number;
  proposer: string;
  date: Date;
  members: Member[];
}

export interface Member {
  [key: string]: string;
}

export interface User {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
}

export interface Question {
  createdAt: Date;
  senderUID: string;
  messageText: string;
}

export interface Reaction {
  createdAt: Date;
  senderUID: string;
  messageText: string;
  sentiment: number;
}

export interface Vote {
  createdAt: Date;
  senderUID: string;
  vote: boolean;
}







