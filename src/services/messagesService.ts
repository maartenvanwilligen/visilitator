/*import node modules*/
import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';

import {map, switchMap} from 'rxjs/operators';
import {combineLatest, defer, of} from "rxjs/index"
import {shareReplay} from 'rxjs/operators';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';

/*import interfaces*/
import {Question, Reaction, User, Vote} from '../interfaces/interface'

/*import services*/
import {AuthService} from "./AuthService";

/*
 * @desc Service voor het afhandelen van de de vragen, reacties, stemmingen (berichten).
 * @desc CRUD taken voor alle berichten.
 * @param authservice -- Authenticatie gebruiker
 * @param db -- Referentie naar firebase database
 * */
@Injectable()
export class MessagesService {

  /*
  * @desc Initialisatie variablen
  * */
  questions: Observable<Question[]> = null;
  users: Observable<User[]> = null;

  batch;

  constructor(public authService: AuthService, private db: AngularFirestore) {
    this.batch = db.firestore.batch();
  }

  /*
  * @desc Verkrijg alle 'vragen' die behoren bij het voorstel. Voeg daarna aan elke 'vraag' de data van de verzender van die 'vraag' toe.
  * @param proposeId -- Het id waarmee het voorstel kan worden gevonden in de database.
  * @return observable -- 1 observable met de data stream van reacties en de verzenders van deze reacties.
  * */
  getQuestions(proposeId: string) {
    return this.db.collection('/questions/' + proposeId + '/messages').valueChanges().pipe(
      this.leftJoinDocument('senderUID', 'users'), //custom operator
      shareReplay(1) //Bij een nieuwe subscriber herhaal eerst de al opgehaalde data stream.
    );
  }

/*
* @desc Verkrijg alle 'reacties' die behoren bij het voorstel. Voeg daarna aan elke 'reactie' de data van de verzender van die 'reactie' toe.
* @param proposeId -- Het id waarmee het voorstel kan worden gevonden in de database.
* @return observable -- 1 observable met de data stream van reacties en de verzenders van deze reacties.
* */
  getReactions(proposeId: string) {
    return this.db.collection('/reactions/' + proposeId + '/messages').valueChanges().pipe(
      this.leftJoinDocument('senderUID', 'users'), //custom operator
      shareReplay(1) //Bij een nieuwe subscriber herhaal eerst de al opgehaalde data stream.
    );
  }

  /*
  * @desc Verkrijg alle 'stemmen' die behoren bij het voorstel. Voeg daarna aan elke 'stem' de data van de verzender van die 'stem' toe.
  * @param proposeId -- Het id waarmee het voorstel kan worden gevonden in de database.
  * @return observable -- 1 observable met de data stream van reacties en de verzenders van deze reacties.
  * */
  getVotes(proposeId: string) {
    return this.db.collection('/votes/' + proposeId + '/messages').valueChanges().pipe(
      //operators -- Met operators kan de collectie worden gemanipuleerd.
      //the operator observes the source observable’s emitted values, transforms them, and returns a new observable of those transformed values
      this.leftJoinDocument('senderUID', 'users'), //custom operator
      shareReplay(1) //Bij een nieuwe subscriber herhaal eerst de al opgehaalde data stream.
    );
  }

  /*
  * @desc Zet een vraag in de firestore database
  * @param proposeId -- Het id waarmee het voorstel, waaraan de vraag is gekoppeld, kan worden gevonden in de database.
  * */
  setQuestion(proposeId, Question) {
    const data: Question = {
      createdAt: new Date(),
      senderUID: Question.senderUID,
      messageText: Question.message
    };

    this.db.collection<any>('/questions/' + proposeId + '/messages').add(data).then(msg => {
    }).catch(function (err) {
      console.log(err)
    });
  }

  /*
  * @desc Zet een reactie in de firestore database
  * @param proposeId -- Het id waarmee het voorstel, waaraan de reactie is gekoppeld, kan worden gevonden in de database.
  * */
  setReaction(proposeId, Reaction) {

    const data: Reaction = {
      createdAt: new Date(),
      senderUID: Reaction.senderUID,
      messageText: Reaction.message,
      sentiment: Reaction.sentiment
    };

    this.db.collection('/reactions/' + proposeId + '/messages').add(data).then(msg => {
    }).catch(function (err) {
      console.log(err);
    });

  }

  /*
 * @desc Zet een vraag in de firestore database
 * @param proposeId -- Het id waarmee het voorstel, waaraan de vraag is gekoppeld, kan worden gevonden in de database.
 * */
  setVote(proposeId, Vote) {
    const data: Vote = {
      createdAt: new Date(),
      senderUID: Vote.senderUID,
      vote: Vote.vote
    };

    this.db.collection('/votes/' + proposeId + '/messages').add(data).then(msg => {
    }).catch(function (err) {
      console.log(err);
    });

  }

  /*
* @desc Een custom operator die twee documenten samenvoegt. Net zoals bij een join query in een sql database.
* @desc De waarde van het veld (field) moet overeenkomen een het id van een document uit een collectie.
* @desc Is er een referentie: Dan joined de functie de twee documenten.
* @example De verzender van een chat bericht is opgeslagen met een referentie id.
* @example Het chat bericht document moet worden samngevoegt met de juiste user document om gebruik te kunnen maken van de foto van de user.
* @param field -- Het referentie veld: het veld waarvan de waarde hetzelde moet zijn als het id van het document dat moet worden gejoined
* @param collection -- De collectie aan documenten die wordt doorzocht.
* @return observable -- observable van source met daarin de gejoinde documenten.
* */
  leftJoinDocument(field, collection) {
    // Source: source observable
    // Defer: Maakt een nieuwe observable aan op het moment dat een observer subscribed.
    return source => defer(() => {

      //Declaratie
      let collectionData;

      // Initialisatie
      //  Map object bevat key -value paren en onthoud de volgorde waarin deze zijn toegevoegd.
      const cache = new Map();

      return source.pipe(
        // Switchmap: Switch van de ene data stream (Dit keer de source stream) naar de andere data stream
        switchMap(data => {
          // Clear cache map
          cache.clear();

          collectionData = data as any[];

          const reads = [];
          let i = 0;


          // Loop door de collectie (data stream) van de source.
          for (const doc of collectionData) {

            // Als de parameter field niet bestaat in het document of al is opgeslagen in de cache ga dan naar de volgende iteratie.
            if (!doc[field] || cache.get(doc[field])) {
              continue;
            }

            // Anders, verkrijg de data van het document waarvan het id refereert aan de waarde van de parameter field.
            // Zoek dit document in de collectie die is opgegeven in paramater collectie.
            reads.push(
              this.db
                .collection(collection)
                .doc(doc[field])
                .valueChanges()
            );
            // Sla het document de document referentie op in cache als key en sla het iteratie nummer op als value. (key-value)
            cache.set(doc[field], i);
            i++;
          }

          // Als de array reads een lengte heeft dan geef combinelatest terug. Anders een lege array
          // Combinelatest: Luistert naar de verschillende observabels (in dit geval de observables in de array reads), maar geeft de laats verkregen dat van alle observables gezamenlijk terug.
          // Wanneer 1 document veranderd(emits), geef dan een array terug met die nieuwe data en de laatst verkregen data van andere documenten.

          return reads.length ? combineLatest(reads) : of([]);
        }),
        // Projecteer elke waarde van de bron. In dit geval is de bron de combinelatest array of de lege array zoals in de source.pipe is bepaald.
        map(joins => {
          // Projecteer elke waarde van de collectionData
          return collectionData.map((v) => {
            // Elk document in de collectionData heeft een referentie met een document uit de joins data. Om te achterhalen welk document uit de joinsdata dat is moet de index worden achterhaald van dit document in de join data
            // Het index nummer kan vergregen worden in de cache, omdat daar de referentie is verstrengeld met het index nummer
            const joinIdx = cache.get(v[field]); // Dit is het index nummer van het document in joins.
            return {...v, [field]: joins[joinIdx] || null}; //return de samengevoegde documenten.
          });
        })
      );
    });
  };

}
