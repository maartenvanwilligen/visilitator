/*import node modules*/
import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Observable";
import {Platform} from "ionic-angular";
import {GooglePlus} from "@ionic-native/google-plus";
import {of} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import UserCredential = firebase.auth.UserCredential;
import * as firebase from 'firebase/app';

/* import interfaces*/
import {User} from '../interfaces/interface'

/**
 * @desc Autorisatie service met google login
 * @pararm Angularfirestore -- de referentie naar de firebase database service
 * @pararm Angularfireauth -- de referentie naar de firebase autorisatie service
 * @pararm Google plus -- google plus login service voor android en ios
 * @pararm Platform -- node moduele om platform te achterhalen
 *
 */

@Injectable()
export class AuthService {

  /*
  * @desc declaratie variablen
  * */
  user: Observable<User>;
  userId: string;

  constructor(private db: AngularFirestore,
              private afAuth: AngularFireAuth,
              private gplus: GooglePlus,
              private platform: Platform) {


    /**
     * @desc Initialisatie van user.
     * @desc Eerst de user uid (google id) verkrijgen uit de firebase authenticatie service. Om vervolgens met dat id de user op te halen uit de firebase database.
     * @desc In firestore autorisatie service is alleen de uid en mail opgeslagen. Om ook gebruik te kunnen maken van de username en foto moet de data van de user worden gehaald uit de firebase database.
     * @desc Bij de initialisatie van de user willen we alle data ( naam, foto, uid ) daarom halen we die op in de met de firestore database service en geven we die terug als observable.
     * @return User observable | null
     */

    this.user = this.afAuth.authState.pipe( // Een pipe neemt gegevens op als invoer en transformeert het naar de gewenste uitvoer

      switchMap(user => { // Van de ene observable overschakelen naar een andere observable
        if (user) {
          this.userId = user.uid;
          return this.db.doc<User>(`users/${user.uid}`).valueChanges()
        } else {
          return of(null)
        }
      })
    )

  }

  /**
   * @desc Wanneer de gebruiker zich authenticeert slaat deze functie de gebruikers data op in de firabase database.
   * @desc Op deze manier kunnen ook de naam en foto worden opgeslagen en gebruikt in de rest van de applicatie.
   * @param User -- user object
   */

  private updateUserData(user) {
    console.log("update user");
    // Sets user data to firestore on login
    const userRef: AngularFirestoreDocument<User> = this.db.doc(`users/${user.uid}`);

    const data: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL
    };

    return userRef.set(data, {merge: true})

  }

  /**
   * @desc Starten google login proces
   * @desc Platform bepalen en user data update
   * @param User -- user object
   */
  async googleLogin() {
    console.log("google login");
    if (this.platform.is('cordova')) {
     await this.nativeGoogleLogin().then(user => {
       console.log("cordova google login");
        return this.updateUserData(user);
      }).catch(error => console.log(error));
    } else {
     await this.webGoogleLogin().then(credential => {
       console.log("web google login");
        return this.updateUserData(credential.user);
      }).catch(error => console.log(error));
    }
  }

  /**
   * @desc Uitloggen met firebase authenticatie service
   */

  signOut() {
    this.afAuth.auth.signOut();
  }

  /**
   * @desc Authentcatie met google plus account voor native (ios, android). Pop-up waar de gebruiker kan inloggen met google account
   * @desc Met google native node module voor ionic
   * @return Promise<firebase.User>
   */

  async nativeGoogleLogin(): Promise<firebase.User> {
    try {
      const gplusUser = await this.gplus.login({
        'webClientId': '192058942062-g8vg2s97oc1utt60nijmm2sj6qttien3.apps.googleusercontent.com', //moet overeenkomen met de firebase console
        'offline': true,
        'scopes': 'profile email'
      });

      return await this.afAuth.auth.signInWithCredential(
        firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken)
      )

    } catch (err) {
      console.log(err)
    }
  }

  /**
   * @desc Authentcatie met google plus account voor web. Pop -up waar gebruiker met google account kan inloggen.
   * @desc Met google native node module voor ionic.
   * @return Promise<firebase.User>
   */

  //voor testen in web omgeving.
  async webGoogleLogin(): Promise<UserCredential> {
    try {
      const provider = new firebase.auth.GoogleAuthProvider();
      return await this.afAuth.auth.signInWithPopup(provider);

    } catch (err) {
      console.log(err)
    }
  }

}
