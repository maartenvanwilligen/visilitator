
/*import node modules*/
import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/combineLatest';

/*import interfaces*/
import {Question, User} from '../interfaces/interface'

/*import services*/
import {AuthService} from "./AuthService";

/**
 * @desc Service voor afhandelen gebruikers logica
 * @pararm Angularfirestore -- de referentie naar de firebase database service
 * @param db -- Referentie naar firebase database
 */
@Injectable()
export class UserService {

  /*
   * Initialisatie variablen
   * */
  questions: Observable<Question[]> = null;
  users: Observable<User[]> = null;

  batch;

  constructor(public authService: AuthService, private db: AngularFirestore) {
    console.log("constructor projectService");
    this.batch = db.firestore.batch();
  }

  getUserById(userId) {
    console.log("getUserById");
    return this.db.collection('users', ref => {
      return ref.where("uid", "==", userId)
    }).valueChanges();
  }

}
