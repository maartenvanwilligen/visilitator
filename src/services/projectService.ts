/*import node modules*/
import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';

import 'rxjs/add/observable/combineLatest';
import {map, switchMap} from 'rxjs/operators';
import {combineLatest, defer, of} from "rxjs/index";
import {shareReplay} from 'rxjs/operators';
import WhereFilterOp = firebase.firestore.WhereFilterOp;

/*import interfaces*/
import {Project, Member} from '../interfaces/interface'

/*import services*/
import {AuthService} from "./AuthService";

/*
 * @desc Service voor het afhandelen van de de voorstellen(projecten)
 * @desc CRUD taken voor 'voorstellen'
 * @param authservice -- Authenticatie gebruiker
 * @param db -- Referentie naar firebase database
 * */
@Injectable()
export class ProjectService {

  /*
   * Declaratie variable
   * */
  projects: Observable<any>;
  batch;


  constructor(public authService: AuthService, private db: AngularFirestore) {
    this.batch = db.firestore.batch();
  }

  /*
   * @desc Verkrijg  'voorstel' aan de hand van voorstel id. Voeg daarna aan het voorstel de data van de verzender van het 'voorstel' toe.
   * @param proposeId -- Het id waarmee het voorstel kan worden gevonden in de database.
   * @return Observable -- 1 observable met de data stream van het voortel en de verzender van het voorstel(proposer)
   * */
  getProjectAndProposer(projectId: string) {

    const project = this.db.collection('projects').doc(projectId).valueChanges();
    return project.pipe(
      this.joinOneDocument({proposer: "users"}) //custom operator
    );

  }

  /*
   * @desc Achterhaal welke rol de gebruiker heeft in het voorstel.
   * @param proposeId -- Het id waarmee het voorstel kan worden gevonden in de database.
   * @param uid -- id van de gebruiker.
   * @return String -- rol van gebruiker in voorstel.
   * */
  async getUsersRoleInProject(projectId: string, uid: string) {

    let refDoc = this.returnRef("projects", projectId);
    let result = await refDoc.ref.get();
    const members: Member[] = await result.get("members");
    let member = await members.filter((members) => {
      console.log("filter");
      return members[uid];
    });
    return await member.map(res => {
      console.log(res[uid]);
      return res[uid];
    });

  }

  /*
  * @desc Verkrijg de voorstellen waarbij de opgegeven gebruiker het voorstel heeft ingediend.
  * @param userId -- id van de gebruiker.
  * @return Observable -- Observable van voorstellen waarbij de gebruiker het voorstel heeft ingediend
  * */
  getProposalsByUserId(userId) {
    return this.db.collection('projects', ref => {
      return ref.where("proposer", "==", userId)
    }).valueChanges().pipe(
      this.joinDocument('proposer', 'users'), //custom operator
      shareReplay(1)
    );
  }

  /*
  * @desc Verkrijg alle voorstellen
  * @return Observable -- Observable van voorstellen en bij elk voorstel de data van de gebruiker die het voorstel heeft ingediend.
  * */
  getAllProposals() {
    return this.db.collection('projects').valueChanges().pipe(
      this.joinDocument('proposer', 'users'), //custom operator
      shareReplay(1)
    );
  }

  /*
 * @desc Zet voorstel in volgende fase
 * */
  updateProjectToNextPhase(projectId: string, nextPhase) {
    return this.db.collection('projects').doc(projectId).update({"step": nextPhase})
  }

  /*
* @desc Zet voorstel in vorige fase
* */
  updateProjectToPreviousPhase(projectId: string, previousPhase) {
    return this.db.collection('projects').doc(projectId).update({"step": previousPhase})
  }

  /*
* @desc Update proposal
* @param proposal -- voorstel
* */
  updateProposal(proposal: Project) {
    this.db.collection('projects').doc(proposal.projectId).update(proposal).then(msg => {
    }).catch(function (err) {
      console.log(err);
    });
  }

  /*
 * @desc Een custom operator die twee documenten samenvoegt. Net zoals bij een join query in een sql database.
 * @desc De waarde van het veld (field) moet overeenkomen met het id van een document uit de opgegeven collectie.
 * @desc Is er een match/referentie: Dan voegt de functie de twee documenten samen tot 1 document.
 * @example Een gebruiker verstuurt een chat bericht. De data van het chat bericht en de data van de gebruiker moeten worden samengevoegd.
 * @param field -- De key van het referentie veld.
 * @param collection -- De collectie waarin gezoch moet worden naar eer referentie.
 * @return observable -- Een enkele Observable met daarin de samengevoegde documenten.
* */

  joinDocument(field, collection) {

    /*
    * Source: Source observable
    * Defer: Maakt een nieuwe Observable aan op het moment dat een Observer subscribed.
    * */

    return source => defer(() => {

      //Declaratie
      let collectionData;

      /*
       * Map object bevat key -value paren
       * Map onthoud de volgorde waarin de paren zijn toegevoegd.
       * */

      const cache = new Map(); // Initialisatie

      return source.pipe(
        // switchMap: Switch van de ene data stream (Een Observable) naar de andere data stream.
        switchMap(data => {

          cache.clear();   // Clear cache Map
          collectionData = data as any[]; // Initialiseer de Collectiondata met de source data.

          const reads = [];
          let i = 0;

          // Loop door de collectie.
          for (const doc of collectionData) {

           /*
           * Als de parameter field niet bestaat in het document ga dan naar de volgende iteratie.
           * Als de field waarde al is opgeslagen in de cache ga dan naar de volgende iteratie.
           * Dit is om een error te voorkomen en om te voorkomen dat dubbele oproep word gedaan naar de database voor een document.
           * */
            if (!doc[field] || cache.get(doc[field])) {
              continue;
            }

            /*
            * Anders, verkrijg de data van het document waarvan het document id hetzelfde is als de waarde van de parameter field.(Referentie)
            * Zoek dit document in de collectie die is opgegeven in paramater collection. (Bijvoorbeeld Users collectie)
            * */

            reads.push(
              this.db
                .collection(collection)
                .doc(doc[field])
                .valueChanges()
            );

            /*
            * Cache (key-value)
            * Voeg de document referentie toe in cache als key en
            * Voeg het iteratie nummer  toe als value.
            * (Iteratie nummer is later nodig om de index te achterhalen)
            * */

            cache.set(doc[field], i);
            i++;
          }

          /*
          * Wanneer de array reads een lengte heeft: dan geef combinelatest terug (array)
          * Anders: Een lege array
          * Combinelatest: Luistert naar de verschillende toegewezen Observabels (In dit geval de Observables in de array reads).
          * Wanneer één Observable emits(veranderd) geeft combinelatest de laatst verkregen waarde/data van alle Observables terug in een array.
          * */

          return reads.length ? combineLatest(reads) : of([]);
        }),

        // Projecteer elke waarde van de bron als joinsData. In dit geval is de bron de cobinelatest array of een lege array.
        map(joinsData => {

          // Projecteer elke waarde van de collectionData
          return collectionData.map((v) => {

            /*
            * Elk document in de collectionData heeft een verwijzing/referentie naar een document uit de joins data.
            * Het juiste document uit joinsData moet toegevoegd worden aan elk document uit collectionData.
            * Het index nummer van het juiste document in joinsDatastaat in de cache.
            * In de cache is de verwijzing/referentie verstrengeld met het index nummer.
            * */

            const joinIndex = cache.get(v[field]); // Dit is het index nummer van het document in join dat toegevoed moet worden.
            return {...v, [field]: joinsData[joinIndex] || null}; //Geef de samengevoegde documenten terug.
          });
        })
      );
    });
  };


  /*
* @desc Een custom operator die twee documenten samenvoegt. Net zoals bij een join query in een sql database. Op basis van een refer
* @param paths: { [key: string]: string } -- key: is het veld waar de referentie/verwijzing naar het andere document te vinden is. Value: De collectie waarin het document gezocht moet worden die overneenkomt met de referentie.
* @return observable -- observable van de samengevoegde documenten.
* */

  joinOneDocument(
    paths: { [key: string]: string }
  ) {
    console.log("docJoin");
    return source =>
      defer(() => {
        let parent;
        const keys = Object.keys(paths);

        return source.pipe(
          switchMap(data => {

            parent = data;
            const docs = keys.map(k => {
              const fullPath = `${paths[k]}/${parent[k]}`;
              return this.db.doc(fullPath).valueChanges();
            });

            return combineLatest(docs);
          }),
          map(arr => {
            const joins = keys.reduce((acc, cur, idx) => {
              return {...acc, [cur]: arr[idx]};
            }, {});

            return {...parent, ...joins};
          })
        );
      });
  };

  /*
* @desc Een functie om een referentie naar de database te maken.
* @return referentie naar database.
* */
  returnRef(collection: string, doc?: string, field?: string, operation?: WhereFilterOp, value?: any) {
    let ref = null;
    if (doc) {
      ref = this.db.collection(collection).doc(doc);
      return ref
    } else {
      if (field && operation && value) {
        ref = this.db.collection(collection, ref => {
          return ref.where(field, operation, value)
        });
        return ref

      } else {
        ref = this.db.collection(collection);
        return ref
      }
    }
  }

}
