import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';

import {HomePage} from '../pages/home/home';
import {TabsPage} from '../pages/tabs/tabs';
import {LoginPage} from "../pages/login/login";
import {NotificationsPage} from "../pages/notifications/notifications";

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {ComponentsModule} from '../components/components.module';

import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFirestoreModule} from '@angular/fire/firestore';


import {GooglePlus} from '@ionic-native/google-plus';
import {AuthService} from "../services/AuthService";
import {ProposeDetailPage} from "../pages/propose-detail/propose-detail";
import {MessagesService} from "../services/messagesService";
import {ProjectService} from "../services/projectService";
import {UserService} from "../services/userService";
import {ProposeDetailPopoverPage} from "../pages/propose-detail-popover/propose-detail-popover";
import {ProposeAddPage} from "../pages/propose-add/propose-add";

const firebaseConfig = {
  // your config
  apiKey: "AIzaSyDrtAgpc-aKwrLhZL8gUBEtZ2eXNa9O-T4",
  authDomain: "visilitator-217811.firebaseapp.com",
  databaseURL: "https://visilitator-217811.firebaseio.com",
  projectId: "visilitator-217811",
  storageBucket: "visilitator-217811.appspot.com",
  messagingSenderId: "192058942062"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    LoginPage,
    NotificationsPage,
    ProposeDetailPage,
    ProposeDetailPopoverPage,
    ProposeAddPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig), // <-- firebase here
    AngularFireAuthModule,
    AngularFirestoreModule,
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    LoginPage,
    NotificationsPage,
    ProposeDetailPage,
    ProposeDetailPopoverPage,
    ProposeAddPage
  ],
  providers: [
    GooglePlus,
    StatusBar,
    SplashScreen,
    MessagesService,
    ProjectService,
    UserService,
    AuthService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
}
