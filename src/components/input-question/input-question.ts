import { Component } from '@angular/core';
import {AuthService} from "../../services/AuthService";
import {NavParams} from "ionic-angular";
import {MessagesService} from "../../services/messagesService";
import {Question} from "../../interfaces/interface";

/**
 * Generated class for the InputQuestionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'input-question',
  templateUrl: 'input-question.html'
})
export class InputQuestionComponent {

  message: string;

  constructor(public MessageService: MessagesService, public authService: AuthService, public navParams: NavParams) {

  }


  /*
  * @desc Verstuur 'vraag'
  * @desc Zet 'vraag' in database
  * */
  submit() {

    const data: Question = {
      createdAt: new Date(),
      senderUID: this.authService.userId,
      messageText: this.message
    };

    this.MessageService.setQuestion(this.navParams.get("projectId"), data );
    this.message = "";
  }

}
