import { Component } from '@angular/core';
import {NavParams} from "ionic-angular";
import {MessagesService} from "../../services/messagesService";
import {Observable} from "rxjs/index";

/**
 * Generated class for the ReactionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'reactions',
  templateUrl: 'reactions.html'
})

export class ReactionsComponent {

  reactions: Observable<any>;

  constructor(public messageService: MessagesService, public navParams: NavParams) {
    console.log('Hello ReactionsComponent Component');

    /*
   * Initialisatie reacties
   * */
    this.reactions = this.messageService.getReactions(this.navParams.get("projectId"));
  }

}
