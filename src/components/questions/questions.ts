import { Component } from '@angular/core';
import {MessagesService} from "../../services/messagesService";
import {Observable} from "rxjs/index";
import {NavParams} from "ionic-angular";


/**
 * Generated class for the QuestionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'questions',
  templateUrl: 'questions.html'
})
export class QuestionsComponent {

  messages: Observable<any>;

  constructor(public messageService: MessagesService, public navParams: NavParams) {
    console.log('Hello QuestionsComponent Component');

    /*
    * Initialisatie messages (vragen)
    * */
    this.messages = this.messageService.getQuestions(this.navParams.get("projectId"));

  }

}
