import { Component } from '@angular/core';
import {NavParams} from "ionic-angular";
import {MessagesService} from "../../services/messagesService";
import {Observable} from "rxjs/index";

/**
 * Generated class for the VotesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'votes',
  templateUrl: 'votes.html'
})
export class VotesComponent {


  votes: Observable<any>;

  constructor(public messageService: MessagesService, public navParams: NavParams) {
    console.log('Hello VotesComponent Component');

    /*
   * Initialisatie stemmen
   * */
    this.votes = this.messageService.getVotes(this.navParams.get("projectId"));
  }

}
