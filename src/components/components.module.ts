import { NgModule } from '@angular/core';

import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
import { GoogleLoginComponent } from './google-login/google-login';
import { QuestionsComponent } from './questions/questions';
import { ReactionsComponent } from './reactions/reactions';
import { VotesComponent } from './votes/votes';
import { IntegrationsComponent } from './integrations/integrations';
import { StepsComponent } from './steps/steps';
import { InputQuestionComponent } from './input-question/input-question';
import { InputReactionComponent } from './input-reaction/input-reaction';
import { InputVoteComponent } from './input-vote/input-vote';

@NgModule({
	declarations: [GoogleLoginComponent,
    QuestionsComponent,
    ReactionsComponent,
    VotesComponent,
    IntegrationsComponent,
    StepsComponent,
    InputQuestionComponent,
    InputReactionComponent,
    InputVoteComponent],
	imports: [
    CommonModule, // <--- for angular directives
    IonicModule  // <--- for ionic components
  ],
	exports: [GoogleLoginComponent,
    QuestionsComponent,
    ReactionsComponent,
    VotesComponent,
    IntegrationsComponent,
    StepsComponent,
    InputQuestionComponent,
    InputReactionComponent,
    InputVoteComponent]
})
export class ComponentsModule {}
