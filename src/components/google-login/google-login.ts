import {Component} from '@angular/core';
import {AuthService} from "../../services/AuthService";
import {NavController} from "ionic-angular";
import {TabsPage} from "../../pages/tabs/tabs";

/**
 * Generated class for the GoogleLoginComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

@Component({
  selector: 'google-login',
  templateUrl: 'google-login.html'
})


export class GoogleLoginComponent {

  constructor(public authService: AuthService, public navCtrl: NavController ) {

  }

  googleLogin() {
      this.authService.googleLogin().then(() => {
      this.navCtrl.push(TabsPage);
    })
  };

  goToApp() {
    console.log("to app" + " " + this.authService.userId);
      this.navCtrl.push(TabsPage)
  }


}
