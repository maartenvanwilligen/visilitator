import { Component } from '@angular/core';
import {AuthService} from "../../services/AuthService";
import {NavParams} from "ionic-angular";
import {MessagesService} from "../../services/messagesService";
import {Vote} from "../../interfaces/interface";

/**
 * Generated class for the InputVoteComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'input-vote',
  templateUrl: 'input-vote.html'
})
export class InputVoteComponent {

  vote: boolean;

  constructor(public MessageService: MessagesService, public authService: AuthService, public navParams: NavParams) {
    console.log('Hello InputVoteComponent Component');

  }

  /*
  * @desc Verstuur 'stem'
  * @desc Zet 'stem' in database
  * */

  submit() {
    const data: Vote = {
      createdAt: new Date(),
      senderUID: this.authService.userId,
      vote: this.vote
    };

    this.MessageService.setVote(this.navParams.get("projectId"), data);
    this.vote = null;
  }

}
