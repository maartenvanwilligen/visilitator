import { Component } from '@angular/core';
import {AuthService} from "../../services/AuthService";
import {NavParams} from "ionic-angular";
import {MessagesService} from "../../services/messagesService";
import {Reaction} from "../../interfaces/interface";

/**
 * Generated class for the InputReactionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'input-reaction',
  templateUrl: 'input-reaction.html'
})
export class InputReactionComponent {

  message: string;
  sentiment: number;

  constructor(public MessageService: MessagesService, public authService: AuthService, public navParams: NavParams) {

  }


  /*
 * @desc Verstuur 'reactie'
 * @desc Zet 'reactie' in database
 * */

  submit() {

    const data: Reaction = {
      createdAt: new Date(),
      senderUID: this.authService.userId,
      messageText: this.message,
      sentiment: this.sentiment
    };

    this.MessageService.setReaction(this.navParams.get("projectId"), data);
    this.message = "";
    this.sentiment = null;
  }

}
