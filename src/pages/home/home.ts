import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {ProposeDetailPage} from "../propose-detail/propose-detail";
import 'rxjs/add/observable/forkJoin'
import {Observable} from 'rxjs';

import {ProjectService} from "../../services/projectService";
import {AuthService} from "../../services/AuthService";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  proposals: Observable<any>;
  proposer: Observable<any>;
  selected;

  constructor(public authService: AuthService, public projectService: ProjectService, public navCtrl: NavController, public navParams: NavParams) {

    this.selected = "all";
    this.showAllProposals();

  }

  /**
   * @desc Ga naar de detail pagina van het opgegeven voorstel.
   * @param projectId -- De door de gebruiker van de applicatie geselecteerde voorstel id
   */
  itemSelected(projectId: string) {
    this.navCtrl.push(ProposeDetailPage, {
        projectId: projectId
      }
    ).then();

  }

  /**
   * @desc Verkrijg alle voorstellen waaraan de gebruiker deelneemt
   */
  showMyProposals() {
    this.proposals = this.projectService.getProposalsByUserId(this.authService.userId);
    this.proposals.subscribe();
  }

  /**
   * @desc Verkrijg alle voorstellen
   */
  showAllProposals() {
    this.proposals = this.projectService.getAllProposals();
    this.proposals.subscribe();
  }


}
