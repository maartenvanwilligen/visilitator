import {Component} from '@angular/core';


import {NotificationsPage} from "../notifications/notifications";
import {HomePage} from "../home/home";
import {NavController} from "ionic-angular";
import {ProposeAddPage} from "../propose-add/propose-add";


@Component({
  templateUrl: 'tabs.html'
})

export class TabsPage {

  /**
   * @desc navigatie initialisatie
   */
  tab1Root = HomePage;
  tab2Root = ProposeAddPage;
  tab3Root = NotificationsPage;
  data;

  constructor(public navCtrl: NavController) {
    /*this.data = {
      navCtrl: this.navCtrl
    }*/
  }


}
