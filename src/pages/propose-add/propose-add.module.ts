import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProposeAddPage } from './propose-add';

@NgModule({
  declarations: [
    ProposeAddPage,
  ],
  imports: [
    IonicPageModule.forChild(ProposeAddPage),
  ],
})
export class ProposeAddPageModule {}
