import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProposeDetailPopoverPage } from './propose-detail-popover';

@NgModule({
  declarations: [
    ProposeDetailPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(ProposeDetailPopoverPage),
  ],
})
export class ProposeDetailPopoverPageModule {}
