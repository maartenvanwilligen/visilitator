import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ProjectService} from "../../services/projectService";

/**
 * Generated class for the ProposeDetailPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-propose-detail-popover',
  templateUrl: 'propose-detail-popover.html',
})
export class ProposeDetailPopoverPage {

  constructor(public navCtrl: NavController,
              public projectService: ProjectService,
              public navParams: NavParams,
             ) {
  }


  /**
   * @desc Zet het voorstel naar de vorige fase
   */
  previousPhase() {
    let previousPhase = null;
    switch(this.navParams.get("projectPhase")) {
      case "clarify": {
        previousPhase = "clarify";
        //statements;
        break;
      }
      case "comment": {
        previousPhase = "clarify";
        break;
      }
      case "vote": {
        previousPhase = "comment";
        break;
      }
      case "integrate": {
        previousPhase = "vote";
        break;
      }

    }

    this.projectService.updateProjectToPreviousPhase(this.navParams.get("projectId"), previousPhase).then(
    );
  }

  /**
   * @desc Zet het voorstel naar de volgende fase
   */
  nextPhase() {
    let nextPhase = null;
    switch(this.navParams.get("projectPhase")) {
      case "clarify": {
        nextPhase = "comment";
        break;
      }
      case "comment": {
        nextPhase = "vote";
        break;
      }
      case "vote": {
        nextPhase = "integrate";
        break;
      }

      case "integrate": {
        nextPhase = "integrate";
        break;
      }
    }

    this.projectService.updateProjectToNextPhase(this.navParams.get("projectId"), nextPhase).then(
    );
  }

  close() {
   this.navCtrl.pop();
  }

}
