import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, PopoverController} from 'ionic-angular';
import {AuthService} from "../../services/AuthService";
import {ProjectService} from "../../services/projectService";
import {Observable} from 'rxjs';
import {ProposeDetailPopoverPage} from "../propose-detail-popover/propose-detail-popover";

/**
 * Generated class for the ProposeDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-propose-detail',
  templateUrl: 'propose-detail.html',
})
export class ProposeDetailPage {

  project: Observable<any>;
  userRole: Observable<any> = null;
  projectId: string;

  // step = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public projectService: ProjectService, public popoverCtrl: PopoverController) {
    this.projectId = navParams.get("projectId");
  }

  ngOnInit() {

    /**
     * @desc Initialisatie van userRole. De gebruikers rol in het project
     */
    this.projectService.getUsersRoleInProject(this.navParams.get("projectId"), this.authService.userId).then(res => {
      this.userRole = Observable.create(function (observer) {
        observer.next(res.toString());
      });
    });

    /**
     * @desc Initialisatie van project(voorstel)
     */
    this.project = this.projectService.getProjectAndProposer(this.navParams.get("projectId"));

  }

  onClickBack() {
    this.navCtrl.pop();
  }

  /**
   * @desc Laat een popover pagina zien op click event.
   * @param myEvent -- click event
   * @param projectPhase -- fase waarin het voorstel zit
   */
  presentPopover(myEvent, projectPhase) {
    let popover = this.popoverCtrl.create(ProposeDetailPopoverPage, {
      projectId: this.navParams.get("projectId"),
      projectPhase: projectPhase
    });
    popover.present({
      ev: myEvent
    });
  }

}
