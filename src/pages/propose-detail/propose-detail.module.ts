import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProposeDetailPage } from './propose-detail';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  declarations: [
    ProposeDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ProposeDetailPage),
    ComponentsModule
  ],
})
export class ProposeDetailPageModule {}
